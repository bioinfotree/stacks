#!/usr/bin/env python
from __future__ import with_statement

import sys
import argparse
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
# provides regular expression matching operations
import re
#from subprocess import Popen, PIPE



def main(arguments):
	sys.argv = arguments
	args = arg_parse()
	
	# all file in memory as a list
	f = sys.stdin.readlines()
		
	for i, line in enumerate(open(args.markers, "r")):
		# only non-empty lines
		if i != 0 and safe_rstrip(line).split("\t")[0] != "":
			# need to scroll all the list
			for line_2 in f:
				# tag as search pattern
				p = re.compile(safe_rstrip(line_2).split("\t")[1])
				# search along all the .markers line
				m = p.search(line)
				if m:
					# stacks tag ID
					chunk_1 = int(line_2.split("\t")[0])
					# rest of the line
					chunk_2 = "\t".join(line_2.split("\t")[1:])
					# add tabs
					sys.stdout.write("%i\t\t\t\t%s" % (chunk_1, chunk_2))

		print safe_rstrip(line)
	return 0




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Merge the output of merge_tags-haplo.py from std-in into a *.markers file from RADtools RADmarkers. The stacks tag with the corresponding haplotype are aligned on top of the .markers line with the same tag. Results to std-out.", prog=sys.argv[0], epilog="michele.vidotto@gmail.com")
    parser.add_argument("--markers", "-m", dest="markers", required=True, help="Path to a *.markers file from RADtools RADmarkers.")
    args = parser.parse_args()
    
    return args


if __name__ == '__main__':
	main(sys.argv)

