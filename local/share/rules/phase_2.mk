### phase_2.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

extern ../phase_1/cstacks_POPS/batch_2.catalog.tags.tsv as STACKS_POP_CAT_TAGS
extern ../phase_1/gen_POPS_fam/batch_2.haplotypes_1.tsv as STACKS_POP_HAPLO

# extern ../../../../radtools/dataset/cluster_distance_5/phase_2/persicus_gualdenstaedtii_nord_caspian.markers as RADT_POP_PERSNC_GUALDNC
# extern ../../../../radtools/dataset/cluster_distance_5/phase_2/baeri_lena_vs_ob.markers as RADT_POP_BAERI_LENA_OB
# extern ../../../../radtools/dataset/cluster_distance_5/phase_2/gueldenstaedtii_azov_vs_caspian.markers as RADT_POP_GUALDNC_AZOV_CASP


extern v2_v3_a_persicus_vs_a_gueldenstaedtii_north_caspian.markers as RADT_POP_PERSNC_GUALDNC
extern v2_v3_a_baeri_lena_vs_ob.markers as RADT_POP_BAERI_LENA_OB
extern v2_v3_a_gueldenstaedtii_azov_vs_caspian.markers as RADT_POP_GUALDNC_AZOV_CASP




$(notdir $(STACKS_POP_CAT_TAGS)): $(STACKS_POP_CAT_TAGS)
	ln -sf $< $@

$(notdir $(STACKS_POP_HAPLO)): $(STACKS_POP_HAPLO)
	ln -sf $< $@


# acronyms
p_g_nc = p_g_nc
b_l_o = b_l_o
g_a_c = g_a_c

trans_table.txt:
	@echo -e -n "acronym\tdescription\t.markers_file\n" > $@ 
	@echo -e -n "$(p_g_nc):\tpersicus_vs_gualdenstaedtii_nord_caspian\t$(abspath $(RADT_POP_PERSNC_GUALDNC))\n" >> $@
	@echo -e -n "$(b_l_o):\tbaeri_lena_river_vs_ob_river\t$(abspath $(RADT_POP_BAERI_LENA_OB))\n" >> $@
	@echo -e -n "$(g_a_c):\tgueldenstaedtii_azov_river_vs_caspian_river\t$(abspath $(RADT_POP_GUALDNC_AZOV_CASP))\n" >> $@



$(p_g_nc).markers: $(RADT_POP_PERSNC_GUALDNC)
	ln -sf $< $@

$(b_l_o).markers: $(RADT_POP_BAERI_LENA_OB)
	ln -sf $< $@

$(g_a_c).markers: $(RADT_POP_GUALDNC_AZOV_CASP)
	ln -sf $< $@



# finds all the RADmarkers tags that are also present in the Stacks output
%.tags.tsv:  %.markers $(notdir $(STACKS_POP_CAT_TAGS))
	_comment () { echo -n ""; }; \
	_comment "grep -P [ACGT]{90} *.markers: print any lines with 90 ACGT characters (-P = Perl regular expression, not a standard grep one"; \
	_comment "cut -f4: only retain the tag sequence"; \
	_comment "grep -f *.tags.tsv > *.stacks use the output of the 'cut -f4' statement as a set of search patterns for grep and pull those lines out of the Stacks catalogue. The -f option can be used to give a file of search patterns to grep, rather than just giving the search patterns on the command line. The output of a previous command can be used as a 'file', so here we are giving all the tags in the RADmarkers output as search patterns. Any lines that match those patterns in the Stacks output will be output to the *.stacks file."; \
	grep -P [ACGT]{90} $< | \
	cut -f4 | \
	grep -f - $^2 > $@

# extracts the corresponding haplotypes for the tags extracted
%.haplotypes_1.tsv: %.tags.tsv $(notdir $(STACKS_POP_HAPLO))
	_comment () { echo -n ""; }; \
	_comment "cut -f3 *.stacks: pulls out field 3 of the stacks output, which is the tag ID."; \
	_comment "awk '{print "^" $0 "\t"}': search for the tag IDs in the haplotypes file. We need to construct a search pattern to do this. We could just search for the tag ID itself, but these IDs may appear elsewhere in the file (say if the ID was 10, then any occurrence of the string '10' would be matched, not just the one in the tag field). So we use awk to put a ^ before the tag ID and a tab character after it. The ^ means 'only match at the beginning of the line'; as tag ID is the first field in the haplotypes output, this is what we want. And the tab character afterwards is there to make sure we don't match 100, 1000 etc when we search for 10."; \
	_comment "grep -f - *.tsv > *.stacks.haplotypes. Same trick as before. Use the search patterns we've created as input to the -f option in grep and look for these patterns in the haplotypes file."; \
	cut -f3 $< | \
	awk '{print "^" $$0 "\t"}' - | \
	grep -f - $^2 > $@


%.stacks-markers:  %.haplotypes_1.tsv %.tags.tsv %.markers
	_comment () { echo -n ""; }; \
	_comment "Merge_tags-haplo combines the lines of a stacks file *.tags.tsv with those of *.haplotypes_1.tsv, which have the same tag ID. Files are first sorted by tag ID."; \
	_comment "Then, the stacks tag combined with the corresponding haplotype are aligned on top of the .markers line that present the same tag."; \
	merge_tags-haplo --haplotypes <(bsort --general-numeric-sort --key=1,1 $<) \
	--tags <(bsort --general-numeric-sort --key=3,3 $^2) | \
	merge_tagshaplo-markers --markers $^3 > $@




# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += trans_table.txt \
	 $(notdir $(STACKS_POP_CAT_TAGS)) \
	 $(notdir $(STACKS_POP_HAPLO)) \
	 $(p_g_nc).stacks-markers \
	 $(b_l_o).stacks-markers \
	 $(g_a_c).stacks-markers


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(notdir $(STACKS_POP_CAT_TAGS)) \
	 $(notdir $(STACKS_POP_HAPLO)) \
	 trans_table.txt \
	 $(p_g_nc).markers \
	 $(p_g_nc).tags.tsv \
	 $(p_g_nc).haplotypes_1.tsv \
	 $(p_g_nc).stacks-markers \
	 $(b_l_o).markers \
	 $(b_l_o).tags.tsv \
	 $(b_l_o).haplotypes_1.tsv \
	 $(b_l_o).stacks-markers \
	 $(g_a_c).markers \
	 $(g_a_c).tags.tsv \
	 $(g_a_c).haplotypes_1.tsv \
	 $(g_a_c).stacks-markers




# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:

######################################################################
### phase_2.mk ends here
