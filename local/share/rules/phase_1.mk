### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

context prj/radtools

#
RAW_ILLUMINA_FASTQ_1 ?=
#
RAW_ILLUMINA_FASTQ_2 ?=
#
BARCODES_TAB ?=
#
CPUS ?=
#
POPS_STACKS-USTACK_OPT ?=
#
AN_FAMILY_DENOVO_MAP-DENOVO_MAP_OPT ?=
#
POPS_DENOVO_MAP-DENOVO_MAP_OPT ?=




raw_illumina_fastq_1_ln.fastq: $(RAW_ILLUMINA_FASTQ_1)
	ln -sf $< $@

raw_illumina_fastq_2_ln.fastq: $(RAW_ILLUMINA_FASTQ_2)
	ln -sf $< $@


# get chunks of the original fastq file for testing purpose
raw_chunk_1.fastq: raw_illumina_fastq_1_ln.fastq 
	get_fastq @1000:50000 <$< > $@

raw_chunk_2.fastq: raw_illumina_fastq_2_ln.fastq 
	get_fastq @1000:50000 <$< > $@


# produces pools file
BARCODES = sturgeons.pools
$(BARCODES): $(BARCODES_TAB)
	bawk ' \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	split(a[3],b,","); \
	for (i in b) \
	{ \
	if (b[i] != "-") \
	{ \
	printf "%s\n", b[i]; \
	} \
	} \
	}  ' $< > $@


BARCODES_MAP = $(addsuffix .map, $(BARCODES))
$(BARCODES_MAP): $(BARCODES_TAB)
	bawk ' \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	split(a[3],b,","); \
	for (i in b) \
	{ \
	if (b[i] != "-") \
	{ \
	printf "%s\t%s_%s_%i\n", b[i], a[1], a[2], i; \
	} \
	} \
	}  ' $< > $@





# create log dir
log:
	mkdir -p $@;





POOLS_DIR = $(basename $(BARCODES))
BARCODE_LENGTH = 5

# macro of common rules for $(POOLS_DIR)
#
# comment are not permitted inside a macro, because variable contents 
# are mantained, so that the comment lines are not discharged.
define run_process_radtags
mkdir -p $(POOLS_DIR); \
process_radtags -1 $< -2 $^2 -b $^3 -e sbfI -q -D -o $(POOLS_DIR); \
mv $(addsuffix .discards, $<) $(addsuffix .discards, $^2) $(POOLS_DIR); \
cd $(POOLS_DIR); \
for i in sample_*.fq_*; do \
bawk -v file=$$i -v barcode_length=$(BARCODE_LENGTH) '!/^[$$,\#+]/ { \
split($$0,a,"\t"); \
tag = substr(file, 8, barcode_length); \
split(file,b,"."); \
ex = b[2]; \
if ( match(a[2], /^AN/) != 0) \
{ \
split(a[2],c,"_"); \
a[2]=c[1] "-" c[2] "_" c[3] "_" c[4]; \
} \
if (a[1] == tag) \
{ \
printf "mv %s %s_%s.%s\n", file, a[2], tag, b[2]; \
} \
}' ../$^4 | bash; \
done; \
mv *.log ../$^5/; \
cd ..; \
touch $@
endef





# test variable value
ifeq ($(TEST),true)
# for testing purpose
$(POOLS_DIR).flag: raw_chunk_1.fastq raw_chunk_2.fastq $(BARCODES) $(BARCODES_MAP) log
	$(run_process_radtags)
else
$(POOLS_DIR).flag: raw_illumina_fastq_1_ln.fastq raw_illumina_fastq_2_ln.fastq $(BARCODES) $(BARCODES_MAP) log
	$(run_process_radtags)

endif





USTACKS_POPS_DIR = ustacks_POPS 
.ONESHELL: # each line in the recipe is execute in one invocation of the shall
# newlines will be included in the recipe command
# WARNING: not use special characters (@,+,-) in the first line. Use comment instead
# you can use the ONESHELL with other shell like: SHELL = /usr/bin/perl
# you can use it in combination with .SHELLFLAGS = -e for causing any failure in the recipe to cause the shell to fail. 
$(USTACKS_POPS_DIR).flag: $(POOLS_DIR).flag log
	mkdir -p $(basename $@); \
	i=1; \
	for file in ./$(basename $<)/Stur-Pop*.fq_1; do \
	FILE_NAME=$${file##*/}; \
	LOG_ID=` \
	echo -n $$FILE_NAME | \
	bawk '!/^[$$,\#+]/ { \
	print substr($$0, 0, 17); \
	}'`; \
	ID="$$((i++))"; \
	ustacks -t fastq -f $$file -o $(basename $@) -i $$ID -p $(CPUS) $(POPS_STACKS-USTACK_OPT) &> ./$^2/ustacks.$$LOG_ID.log; \
	done; \
	touch $@


USTACKS_AN_DIR =  ustacks_AN_fam
$(USTACKS_AN_DIR).flag: $(POOLS_DIR).flag log
	mkdir -p $(basename $@); \
	i=1; \
	for file in ./$(basename $<)/AN-*_*_*_*.fq_1; do \
	FILE_NAME=$${file##*/}; \
	LOG_ID=` \
	echo -n $$FILE_NAME | \
	bawk '!/^[$$,\#+]/ { \
	print substr($$0, 0, 13); \
	}'`; \
	ID="$$((i++))"; \
	ustacks -t fastq -f $$file -o $(basename $@) -i $$ID -p $(CPUS) $(AN_STACKS-USTACK_OPT) &> ./$^2/ustacks.$$LOG_ID.log; \
	done; \
	touch $@




CSTACKS_AN_DIR = cstacks_AN_fam
AN_BATCH_ID = 1
$(CSTACKS_AN_DIR).flag: $(USTACKS_AN_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	i=1; \
	_comment "prepare catalog statement using only naccarii parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$(basename $<)/AN-P*_*_*_*.tags.tsv; do \
	ID="$$((i++))"; \
	FILE=$${PARENTS_FILE##*/}; \
	FILE_NAME=$${FILE%.*.*}; \
	PARENTS_STAT="$$PARENTS_STAT -s ./$(basename $<)/$$FILE_NAME -S $$ID"; \
	done; \
	echo -e "Analizing parentals..\n$$PARENTS_STAT"; \
	cstacks -b $(AN_BATCH_ID) -o $(basename $@) -m $$PARENTS_STAT -p $(CPUS) &> ./$^2/cstacks.AN_fam.log; \
	touch $@


CSTACKS_POPS_DIR = cstacks_POPS
POPS_BATCH_ID = 2
$(CSTACKS_POPS_DIR).flag: $(USTACKS_POPS_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	i=1; \
	_comment "prepare catalog statement for all pops as parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$(basename $<)/Stur-Pop*_*_*_*.tags.tsv; do \
	ID="$$((i++))"; \
	FILE=$${PARENTS_FILE##*/}; \
	FILE_NAME=$${FILE%.*.*}; \
	PARENTS_STAT="$$PARENTS_STAT -s ./$(basename $<)/$$FILE_NAME -S $$ID"; \
	done; \
	echo -e "Analizing parentals..\n$$PARENTS_STAT"; \
	cstacks -b $(POPS_BATCH_ID) -o $(basename $@) -m $$PARENTS_STAT -p $(CPUS) &> ./$^2/cstacks.POPS.log; \
	touch $@




SSTACKS_AN_DIR = sstacks_AN_fam
$(SSTACKS_AN_DIR).flag: $(USTACKS_AN_DIR).flag $(CSTACKS_AN_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	i=1; \
	_comment "interrogate the catalog, using both offsprings..."; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$(basename $<)/AN-O*_*_*_*.tags.tsv; do \
	ID="$$((i++))"; \
	FILE=$${OFFS_FILE##*/}; \
	FILE_NAME=$${FILE%.*.*}; \
	OFFS_STAT="-s ./$(basename $<)/$$FILE_NAME -S $$ID"; \
	echo -e "Interrogating with offspring... \n$$OFFS_STAT"; \
	sstacks -b $(AN_BATCH_ID) -c ./$(basename $^2)/batch_$(AN_BATCH_ID) -o $(basename $@) $$OFFS_STAT -p $(CPUS) &> ./$^3/sstacks.$$FILE_NAME.log; \
	done; \
	_comment "...and parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$(basename $<)/AN-P*_*_*_*.tags.tsv; do \
	ID="$$((i++))"; \
	FILE=$${PARENTS_FILE##*/}; \
	FILE_NAME=$${FILE%.*.*}; \
	PARENTS_STAT="-s ./$(basename $<)/$$FILE_NAME -S $$ID"; \
	echo -e "Interrogating with parent... \n$$PARENTS_STAT"; \
	sstacks -b $(AN_BATCH_ID) -c ./$(basename $^2)/batch_$(AN_BATCH_ID) -o $(basename $@) $$PARENTS_STAT -p $(CPUS) &> ./$^3/sstacks.$$FILE_NAME.log; \
	done; \
	touch $@




SSTACKS_POPS_DIR = sstacks_POPS
$(SSTACKS_POPS_DIR).flag: $(USTACKS_POPS_DIR).flag $(CSTACKS_POPS_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	i=1; \
	_comment "interrogate the catalog, using all pops"; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$(basename $<)/Stur-Pop*_*_*_*.tags.tsv; do \
	ID="$$((i++))"; \
	FILE=$${OFFS_FILE##*/}; \
	FILE_NAME=$${FILE%.*.*}; \
	OFFS_STAT="$OFFS_STAT -s ./$(basename $<)/$$FILE_NAME -S $$ID"; \
	echo -e "Analizing offspring... \n$$OFFS_STAT"; \
	sstacks -b $(POPS_BATCH_ID) -c ./$(basename $^2)/batch_$(POPS_BATCH_ID) -o $(basename $@) $$OFFS_STAT -p $(CPUS) &> ./$^3/sstacks.$$FILE_NAME.log; \
	done; \
	touch $@



# use -c option?
GEN_AN_DIR = gen_AN_fam
$(GEN_AN_DIR).flag: $(USTACKS_AN_DIR).flag $(CSTACKS_AN_DIR).flag $(SSTACKS_AN_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	ln -sf -t . ../$(basename $<)/*.tsv; \
	ln -sf -t . ../$(basename $^2)/*.tsv; \
	ln -sf -t . ../$(basename $^3)/*.tsv; \
	genotypes -b $(AN_BATCH_ID) -r 1 -P . -m 0 &> ../$^4/genotypes.AN_fam.log; \
	cd ..; \
	touch $@



GEN_POPS_DIR = gen_POPS_fam
$(GEN_POPS_DIR).flag: $(USTACKS_POPS_DIR).flag $(CSTACKS_POPS_DIR).flag $(SSTACKS_POPS_DIR).flag log
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	ln -sf -t . ../$(basename $<)/*.tsv; \
	ln -sf -t . ../$(basename $^2)/*.tsv; \
	ln -sf -t . ../$(basename $^3)/*.tsv; \
	genotypes -b $(POPS_BATCH_ID) -r 1 -P . -m 0 &> ../$^4/genotypes.POPS.log; \
	cd ..; \
	touch $@


###########################################################









# NOT USED
##########################################################
AN_family_denovo_map: $(POOLS_DIR)
	_comment () { echo -n ""; }; \
	mkdir -p $@; \
	_comment "get system data"; \
	DATE=$$(date +"%m-%d-%Y"); \
	_comment "prepare denovo_map statement for parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$</AN-P*_*_*_*.fq_1; do \
	PARENTS_STAT="$$PARENTS_STAT -p $$PARENTS_FILE "; \
	done; \
	_comment "prepare denovo_map statement for offsprings"; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$</AN-O*_*_*_*.fq_1; do \
	OFFS_STAT="$$OFFS_STAT -r $$OFFS_FILE "; \
	done; \
	denovo_map $$PARENTS_STAT $$OFFS_STAT -o $@ -T $(CPUS) -b naccary_family_radtag -D "naccarii family RAD-TAGS" -a $$DATE -S $(AN_FAMILY_DENOVO_MAP-DENOVO_MAP_OPT);



POPS_denovo_map: $(POOLS_DIR)
	_comment () { echo -n ""; }; \
	mkdir -p $@; \
	_comment "get system data"; \
	DATE=$$(date +"%m-%d-%Y"); \
	_comment "prepare denovo_map statement for all pops as parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$</Stur-Pop*.fq_1; do \
	PARENTS_STAT="$$PARENTS_STAT -p $$PARENTS_FILE "; \
	done; \
	_comment "prepare denovo_map statement for all pops as offsprings"; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$</Stur-Pop*.fq_1; do \
	OFFS_STAT="$$OFFS_STAT -r $$OFFS_FILE "; \
	done; \
	denovo_map $$PARENTS_STAT $$OFFS_STAT -o $@ -T $(CPUS) -b sturgeons_pops_radtag -D "sturgeons populations RAD-TAGS" -a $$DATE -S $(POPS_DENOVO_MAP-DENOVO_MAP_OPT);
##########################################################




# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += log \
	 $(POOLS_DIR).flag \
	 $(USTACKS_POPS_DIR).flag \
	 $(USTACKS_AN_DIR).flag \
	 $(CSTACKS_AN_DIR).flag \
	 $(CSTACKS_POPS_DIR).flag \
	 $(SSTACKS_AN_DIR).flag \
	 $(SSTACKS_POPS_DIR).flag \
	 $(GEN_AN_DIR).flag \
         $(GEN_POPS_DIR).flag


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += raw_illumina_fastq_1_ln.fastq \
	 raw_illumina_fastq_2_ln.fastq \
	 raw_illumina_fastq_1_ln.fastq.discards \
	 raw_illumina_fastq_2_ln.fastq.discards \
	 raw_chunk_1.fastq \
	 raw_chunk_2.fastq



# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -r log \
	$(RM) -r $(POOLS_DIR)* \
	$(RM) -r $(USTACKS_POPS_DIR)* \
	$(RM) -r $(USTACKS_AN_DIR)* \
	$(RM) -r $(CSTACKS_AN_DIR)* \
	$(RM) -r $(CSTACKS_POPS_DIR)* \
	$(RM) -r $(SSTACKS_AN_DIR)* \
	$(RM) -r $(SSTACKS_POPS_DIR)* \
	$(RM) -r $(GEN_AN_DIR)* \
	$(RM) -r $(GEN_POPS_DIR)*

######################################################################
### phase_1.mk ends here
