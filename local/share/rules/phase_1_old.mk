### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

#
RAW_ILLUMINA_FASTQ_1 ?=
#
RAW_ILLUMINA_FASTQ_2 ?=
#
BARCODES_TAB ?=
#
CPUS ?=
#
POPS_STACKS-USTACK_OPT ?=
#
AN_FAMILY_DENOVO_MAP-DENOVO_MAP_OPT ?=
#
POPS_DENOVO_MAP-DENOVO_MAP_OPT ?=




raw_illumina_fastq_1_ln.fastq: $(RAW_ILLUMINA_FASTQ_1)
	ln -sf $< $@

raw_illumina_fastq_2_ln.fastq: $(RAW_ILLUMINA_FASTQ_2)
	ln -sf $< $@


# get chunks of the original fastq file for testing purpose
raw_chunk_1.fastq: raw_illumina_fastq_1_ln.fastq 
	get_fastq @1000:50000 <$< > $@

raw_chunk_2.fastq: raw_illumina_fastq_2_ln.fastq 
	get_fastq @1000:50000 <$< > $@


# produces pools file
BARCODES = sturgeons.pools
$(BARCODES): $(BARCODES_TAB)
	bawk ' \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	split(a[3],b,","); \
	for (i in b) \
	{ \
	if (b[i] != "-") \
	{ \
	printf "%s\n", b[i]; \
	} \
	} \
	}  ' $< > $@


BARCODES_MAP = $(addsuffix .map, $(BARCODES))
$(BARCODES_MAP): $(BARCODES_TAB)
	bawk ' \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	split(a[3],b,","); \
	for (i in b) \
	{ \
	if (b[i] != "-") \
	{ \
	printf "%s\t%s_%s_%i\n", b[i], a[1], a[2], i; \
	} \
	} \
	}  ' $< > $@









POOLS_DIR = $(basename $(BARCODES))
BARCODE_LENGTH = 5

# macro of common rules for $(POOLS_DIR)
#
# comment are not permitted inside a macro, because variable contents 
# are mantained, so that the comment lines are not discharged.
define run_process_radtags
mkdir -p $@; \
process_radtags -1 $< -2 $^2 -b $^3 -e sbfI -q -D -o $@; \
mv $(addsuffix .discards, $<) $(addsuffix .discards, $^2) $@; \
cd $@; \
for i in sample_*.fq_*; do \
bawk -v file=$$i -v barcode_length=$(BARCODE_LENGTH) '!/^[$$,\#+]/ { \
split($$0,a,"\t"); \
tag = substr(file, 8, barcode_length); \
split(file,b,"."); \
ex = b[2]; \
if ( match(a[2], /^AN/) != 0) \
{ \
split(a[2],c,"_"); \
a[2]=c[1] "-" c[2] "_" c[3] "_" c[4]; \
} \
if (a[1] == tag) \
{ \
printf "mv %s %s_%s.%s\n", file, a[2], tag, b[2]; \
} \
}' ../$^4 | bash; \
done; \
cd ..;
endef


# test variable value
ifeq ($(TEST),true)
# for testing purpose
$(POOLS_DIR): raw_chunk_1.fastq raw_chunk_2.fastq $(BARCODES) $(BARCODES_MAP)
	$(run_process_radtags)
else

# real    36m21.391s
# user    15m55.080s
# sys     1m45.930s
$(POOLS_DIR): raw_illumina_fastq_1_ln.fastq raw_illumina_fastq_2_ln.fastq $(BARCODES) $(BARCODES_MAP)
	$(run_process_radtags)

endif







POPS_STACKS_DIR = pops_stacks
# Special target that specify that all recipe lines for each target will be provided to a single invocation of the shell. 

# It can appears anywhere in the makefile
.ONESHELL:

$(POPS_STACKS_DIR): $(POOLS_DIR)
	mkdir -p $@; \
	for file in ./$</Stur-Pop*.fq_1; do \
	FILE_NAME=$${file##*/}; \
	ID=` \
	echo -n $$FILE_NAME | \
	bawk '!/^[$$,\#+]/ { \
	print substr($$0, 0, 17); \
	}'`; \
	ustacks -t fastq -f $$file -o $@ -i $$ID -p $(CPUS) $(POPS_STACKS-USTACK_OPT); \
	done;


AN_STACKS_DIR = AN_stacks
$(AN_STACKS_DIR): $(POOLS_DIR)
	mkdir -p $@; \
	for file in ./$</AN-*_*_*_*.fq_1; do \
	FILE_NAME=$${file##*/}; \
	ID=` \
	echo -n $$FILE_NAME | \
	bawk '!/^[$$,\#+]/ { \
	print substr($$0, 0, 13); \
	}'`; \
	ustacks -t fastq -f $$file -o $@ -i $$ID -p $(CPUS) $(AN_STACKS-USTACK_OPT); \
	done;



AN_family_denovo_map: $(POOLS_DIR)
	_comment () { echo -n ""; }; \
	mkdir -p $@; \
	_comment "get system data"; \
	DATE=$$(date +"%m-%d-%Y"); \
	_comment "prepare denovo_map statement for parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$</AN-P*_*_*_*.fq_1; do \
	PARENTS_STAT="$$PARENTS_STAT -p $$PARENTS_FILE "; \
	done; \
	_comment "prepare denovo_map statement for offsprings"; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$</AN-O*_*_*_*.fq_1; do \
	OFFS_STAT="$$OFFS_STAT -r $$OFFS_FILE "; \
	done; \
	denovo_map $$PARENTS_STAT $$OFFS_STAT -o $@ -T $(CPUS) -b naccary_family_radtag -D "naccarii family RAD-TAGS" -a $$DATE -S $(AN_FAMILY_DENOVO_MAP-DENOVO_MAP_OPT);



POPS_denovo_map: $(POOLS_DIR)
	_comment () { echo -n ""; }; \
	mkdir -p $@; \
	_comment "get system data"; \
	DATE=$$(date +"%m-%d-%Y"); \
	_comment "prepare denovo_map statement for all pops as parents"; \
	PARENTS_STAT=""; \
	for PARENTS_FILE in ./$</Stur-Pop*.fq_1; do \
	PARENTS_STAT="$$PARENTS_STAT -p $$PARENTS_FILE "; \
	done; \
	_comment "prepare denovo_map statement for all pops as offsprings"; \
	OFFS_STAT=""; \
	for OFFS_FILE in ./$</Stur-Pop*.fq_1; do \
	OFFS_STAT="$$OFFS_STAT -r $$OFFS_FILE "; \
	done; \
	denovo_map $$PARENTS_STAT $$OFFS_STAT -o $@ -T $(CPUS) -b sturgeons_pops_radtag -D "sturgeons populations RAD-TAGS" -a $$DATE -S $(POPS_DENOVO_MAP-DENOVO_MAP_OPT);





# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += raw_illumina_fastq_1_ln.fastq \
	 raw_illumina_fastq_2_ln.fastq \
	 raw_chunk_1.fastq \
	 raw_chunk_2.fastq \
	 $(BARCODES) \
	 $(POOLS_DIR) \
	 $(BARCODES_MAP) \
	 AN_family_denovo_map \
	 POPS_denovo_map

	 #$(POPS_STACKS_DIR) \
	 #$(AN_STACKS_DIR) \


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += raw_illumina_fastq_1_ln.fastq \
	 raw_illumina_fastq_2_ln.fastq \
	 raw_illumina_fastq_1_ln.fastq.discards \
	 raw_illumina_fastq_2_ln.fastq.discards \
	 raw_chunk_1.fastq \
	 raw_chunk_2.fastq \
	 $(BARCODES) \
	 $(BARCODES_MAP) \

	 

# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	rm -rf $(POOLS_DIR)
	rm -rf $(POPS_STACKS_DIR)
	rm -rf $(AN_STACKS_DIR)
	rm -rf AN_family_denovo_map
	rm -rf POPS_denovo_map

######################################################################
### phase_1.mk ends here
